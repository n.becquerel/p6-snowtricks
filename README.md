# SnowTricks

[![pipeline status](https://gitlab.com/n.becquerel/p6-snowtricks/badges/master/pipeline.svg)](https://gitlab.com/n.becquerel/p6-snowtricks/-/commits/master)
[![coverage report](https://gitlab.com/n.becquerel/p6-snowtricks/badges/master/coverage.svg)](https://gitlab.com/n.becquerel/p6-snowtricks/-/commits/master)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/0ef13732250b4ddb82472205d19def9e)](https://www.codacy.com/gl/n.becquerel/p6-snowtricks/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=n.becquerel/p6-snowtricks&amp;utm_campaign=Badge_Grade)

## Installation guide

### Install dependencies
Install php dependencies. Run : 
```
composer install
```
Then install frontend dependencies:
```
yarn install
```

### Setting up database
First you need to create database. Run : 
``` bash
php bin/console doctrine:database:create
```
Then load schema with migrations: 
``` bash 
php bin/console doctrine:migration:migrate -n
```
Then load initial dataset: 
``` bash 
php bin/console doctrine:fixtures:load -n --group=app
```

### Building assets
For the css and js files getting versionned for production run:
```
yarn encore prod
```
