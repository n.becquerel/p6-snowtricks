<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201106155802 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE trick_photo ADD user_id INT DEFAULT NULL, ADD discriminator VARCHAR(255) NOT NULL, CHANGE trick_id trick_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE trick_photo ADD CONSTRAINT FK_DF9884A7A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_DF9884A7A76ED395 ON trick_photo (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE trick_photo DROP FOREIGN KEY FK_DF9884A7A76ED395');
        $this->addSql('DROP INDEX IDX_DF9884A7A76ED395 ON trick_photo');
        $this->addSql('ALTER TABLE trick_photo DROP user_id, DROP discriminator, CHANGE trick_id trick_id INT NOT NULL');
    }
}
