<?php

namespace App\Tests\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControllerTest extends WebTestCase
{
    // LOGIN
    public function testLoginPageIsUp()
    {
        $client = static::createClient();
        $client->request('GET', '/login');
        $this->assertResponseStatusCodeSame(200);
    }

    public function testWrongLogin()
    {
        $client = static::createClient();
        $client->request('GET', '/login');
        $client->submitForm('Connexion', [
            'email' => 'test@example.com',
            'password' => 'test'
        ]);
        $this->assertResponseRedirects('/login');
        $client->followRedirect();
        $this->assertSelectorExists('.alert.alert-danger');
    }

    public function testLoginSuccessfull()
    {
        $client = static::createClient();
        $client->request('GET', '/login');
        $client->submitForm('Connexion', [
            'email' => 'main@snowtricks.com',
            'password' => 'mainpassword'
        ]);
        $this->assertResponseRedirects('/');
        $client->followRedirect();
        $this->assertSelectorExists('.toast.toast-success');
    }

    public function testLoginWithUnverifiedUser()
    {
        $client = static::createClient();
        $client->request('GET', '/login');
        $client->submitForm('Connexion', [
            'email' => 'disabled@snowtricks.com',
            'password' => 'disabledpassword'
        ]);
        $this->assertResponseRedirects('/');
        $client->followRedirect();
        $this->assertSelectorExists('.toast.toast-success');
    }
    // LOGIN

    // RESET PASSWORD REQUEST
    public function testRequestNewPasswordPageIsUp()
    {
        $client = static::createClient();
        $client->request('GET', '/forgot-password');
        $this->assertResponseStatusCodeSame(200);

        $client->request('GET', '/login');
        $client->clickLink('Mot de passe oublié ?');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Changer de mot de passe');
    }

    public function testRequestNewPasswordWithInvalidEmail()
    {
        $client = static::createClient();
        $client->enableProfiler();
        $client->request('GET', '/forgot-password');
        $client->submitForm('Valider', [
            'forgot_password[email]' => 'test@'
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertEmailCount(0);
        $this->assertContains('Adresse email invalide.', $client->getResponse()->getContent());
    }

    public function testRequestNewPasswordValidEmailWithNoAccount()
    {
        $client = static::createClient();
        $client->enableProfiler();
        $client->request('GET', '/forgot-password');
        $client->submitForm('Valider', [
            'forgot_password[email]' => 'test999@test.com'
        ]);
        $this->assertEmailCount(0);
        $this->assertResponseRedirects('/');
        $client->followRedirect();
        $this->assertSelectorTextContains(
            '.toast.toast-success',
            'Si un compte est associé à l\'adresse email saisie, vous allez recevoir un email contenant' .
            ' les instructions à suivre.'
        );
    }

    public function testRequestNewPasswordValidEmailExistingAccount()
    {
        $client = static::createClient();
        $client->enableProfiler();
        $client->request('GET', '/forgot-password');
        $client->submitForm('Valider', [
            'forgot_password[email]' => 'main@snowtricks.com'
        ]);
        $this->assertEmailCount(1);
        $this->assertResponseRedirects('/');
        $client->followRedirect();
        $this->assertSelectorTextContains(
            '.toast.toast-success',
            'Si un compte est associé à l\'adresse email saisie, vous allez recevoir un email contenant' .
            ' les instructions à suivre.'
        );
    }
    // RESET PASSWORD REQUEST

    // RESET PASSWORD
    public function testResetPasswordGoodToken()
    {
        $client = static::createClient();
        $client->request('GET', '/reset-password/gBq7hsNbi91OKeT8INSzl811laX6kZQpsPRoQ9x1L8g');
        $this->assertResponseStatusCodeSame(200);
        $this->assertSelectorTextContains('h1', 'Changer votre mot de passe');
    }

    public function testResetPasswordNoToken()
    {
        $client = static::createClient();
        $client->request('GET', '/reset-password');
        $this->assertResponseStatusCodeSame(404);
    }

    public function testResetPasswordWrongToken()
    {
        $client = static::createClient();
        $client->request('GET', '/reset-password/test');
        $this->assertResponseRedirects('/');
        $client->followRedirect();
        $this->assertSelectorTextContains(
            '.toast.toast-danger',
            'Impossible de changer le mot de passe de ce compte.'
        );
    }

    public function testResetPasswordSubmitBlank()
    {
        $client = static::createClient();
        $client->request('GET', '/reset-password/gBq7hsNbi91OKeT8INSzl811laX6kZQpsPRoQ9x1L8g');
        $client->submitForm('Changer mon mot de passe');
        $this->assertResponseIsSuccessful();
        $this->assertContains(
            'Veuillez saisir un nouveau mot de passe.',
            $client->getResponse()->getContent()
        );
    }

    public function testResetPasswordSubmitTooShortPassword()
    {
        $client = static::createClient();
        $client->request('GET', '/reset-password/gBq7hsNbi91OKeT8INSzl811laX6kZQpsPRoQ9x1L8g');
        $client->submitForm('Changer mon mot de passe', [
            'reset_password[plainPassword]' => '1'
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertContains(
            'Le mot de passe doit faire au moins 8 caractères.',
            $client->getResponse()->getContent()
        );
    }

    public function testResetPasswordValidData()
    {
        $client = static::createClient();
        $client->request('GET', '/reset-password/gBq7hsNbi91OKeT8INSzl811laX6kZQpsPRoQ9x1L8g');
        $client->submitForm('Changer mon mot de passe', [
            'reset_password[plainPassword]' => 'password'
        ]);
        $this->assertResponseRedirects('/login');
        $client->followRedirect();
        $this->assertSelectorTextContains(
            '.toast.toast-success',
            'Mot de passe modifié.'
        );
    }
    // RESET PASSWORD
}
