<?php

namespace App\Tests\Functional\Controller;

use App\Repository\TrickRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class TrickControllerTest extends WebTestCase
{
    public function testNewTrickPageIsUpWhenLogged()
    {
        $client = static::createClient();
        $client->request('GET', '/tricks/new');
        $this->assertResponseRedirects('/login');

        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $client->request('GET', '/tricks/new');
        $this->assertResponseIsSuccessful();
    }

    // NEW TRICK
    public function testSubmitFormBlankName()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $client->request('GET', '/tricks/new');

        $client->submitForm('Ajouter', [
            'trick[name]' => '',
            'trick[description]' => 'test',
            'trick[trickGroup]' => 1
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('.form-error-message', 'Veuillez saisir le nom du trick');
    }

    public function testSubmitFormBlankDescription()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $client->request('GET', '/tricks/new');

        $client->submitForm('Ajouter', [
            'trick[name]' => 'test',
            'trick[description]' => '',
            'trick[trickGroup]' => 1
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('.form-error-message', 'Veuillez saisir la description du trick');
    }

    public function testSubmitFormBlankTrickGroup()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $client->request('GET', '/tricks/new');

        $client->submitForm('Ajouter', [
            'trick[name]' => 'test',
            'trick[description]' => 'test',
            'trick[trickGroup]' => ''
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains(
            '.form-error-message',
            'Veuillez choisir une catégorie pour le trick'
        );
    }

    public function testSubmitFormValidData()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $client->request('GET', '/tricks/new');

        $client->submitForm('Ajouter', [
            'trick[name]' => 'test',
            'trick[description]' => 'test',
            'trick[trickGroup]' => 1
        ]);
        $this->assertResponseRedirects();
        $client->followRedirect();
        $this->assertSelectorTextContains('.toast.toast-success', 'Trick crée !');
    }

    public function testUniqueTrickByName()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $client->request('GET', '/tricks/new');

        $client->submitForm('Ajouter', [
            'trick[name]' => 'Super test',
            'trick[description]' => 'test',
            'trick[trickGroup]' => 1
        ]);

        $client->request('GET', '/tricks/new');
        $client->submitForm('Ajouter', [
            'trick[name]' => 'Super test',
            'trick[description]' => 'test',
            'trick[trickGroup]' => 1
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains(
            '.form-error-message',
            'Un trick possédant le même nom existe déjà. Veuillez choisir un autre nom.'
        );

        $client->request('GET', '/tricks/new');
        $client->submitForm('Ajouter', [
            'trick[name]' => 'Super tést',
            'trick[description]' => 'test',
            'trick[trickGroup]' => 1
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains(
            '.form-error-message',
            'Un trick possédant le même nom existe déjà. Veuillez choisir un autre nom.'
        );
    }

    // FILE COLLECTION
    public function testSubmitFormWithMultipleFiles()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $crawler = $client->request('GET', '/tricks/new');
        $form = $crawler->filter('button[type="submit"]')->form();
        $values = $form->getPhpValues();
        $values['trick']['name'] = 'test1';
        $values['trick']['description'] = 'test1';
        $values['trick']['trickGroup'] = '1';
        $values['trick']['photos'][0]['file'] = $this->createPhoto('air.jpeg', '1.jpeg');
        $values['trick']['photos'][1]['file'] = $this->createPhoto('air2.jpeg', '2.jpeg');
        $client->request('POST', '/tricks/new', $values, $form->getPhpFiles());

        $this->assertResponseRedirects();
        $crawler = $client->followRedirect();
        $this->assertCount(2, $crawler->filter('.photo-li'));
        $this->assertSelectorTextContains('.toast.toast-success', 'Trick crée !');
    }

    public function testSubmitFormWithBlankPhotos()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $crawler = $client->request('GET', '/tricks/new');
        $form = $crawler->filter('button[type="submit"]')->form();
        $values = $form->getPhpValues();
        $values['trick']['name'] = 'test1';
        $values['trick']['description'] = 'test1';
        $values['trick']['trickGroup'] = '1';
        $values['trick']['photos'][0]['file'] = '';
        $values['trick']['photos'][1]['file'] = '';
        $client->request('POST', '/tricks/new', $values, $form->getPhpFiles());

        $this->assertResponseRedirects();
        $crawler = $client->followRedirect();
        $this->assertCount(0, $crawler->filter('.photo-li'));
        $this->assertSelectorTextContains('.toast.toast-success', 'Trick crée !');
    }

    public function testSubmitFormWithInvalidPhotoNotImage()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $crawler = $client->request('GET', '/tricks/new');
        $form = $crawler->filter('button[type="submit"]')->form();
        $values = $form->getPhpValues();
        $values['trick']['name'] = 'test1';
        $values['trick']['description'] = 'test1';
        $values['trick']['trickGroup'] = '1';
        $values['trick']['photos'][0]['file'] = $this->createPhoto('test.php', '1.php');
        $client->request('POST', '/tricks/new', $values, $form->getPhpFiles());

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains(
            '.form-error-message',
            'Le fichier doit être une image (ex: jpeg, png etc...)'
        );
    }
    // FILE COLLECTION

    // VIDEO COLLECTION
    public function testSubmitFormWithMultipleVideos()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $crawler = $client->request('GET', '/tricks/new');
        $form = $crawler->filter('button[type="submit"]')->form();
        $values = $form->getPhpValues();
        $values['trick']['name'] = 'test1';
        $values['trick']['description'] = 'test1';
        $values['trick']['trickGroup'] = '1';
        $values['trick']['videos'][0]['fullUrl'] = 'https://www.youtube.com/watch?v=_zNd0jn4CKk';
        $values['trick']['videos'][1]['fullUrl'] = 'https://www.youtube.com/watch?v=SR3em6TrgVM';
        $values['trick']['videos'][2]['fullUrl'] = '';
        $client->request('POST', '/tricks/new', $values);

        $this->assertResponseRedirects();
        $crawler = $client->followRedirect();
        $this->assertCount(2, $crawler->filter('.video-li'));
        $this->assertSelectorTextContains('.toast.toast-success', 'Trick crée !');
    }

    public function testSubmitFormWithMultipleVideosIncompleteLinks()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $crawler = $client->request('GET', '/tricks/new');
        $form = $crawler->filter('button[type="submit"]')->form();
        $values = $form->getPhpValues();
        $values['trick']['name'] = 'test1';
        $values['trick']['description'] = 'test1';
        $values['trick']['trickGroup'] = '1';
        $values['trick']['videos'][0]['fullUrl'] = 'www.youtube.com/watch?v=_zNd0jn4CKk';
        $client->request('POST', '/tricks/new', $values);

        $this->assertResponseRedirects();
        $crawler = $client->followRedirect();
        $this->assertCount(1, $crawler->filter('.video-li'));
        $this->assertSelectorTextContains('.toast.toast-success', 'Trick crée !');
    }

    public function testSubmitFormInvalidNotYoutubeLink()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $crawler = $client->request('GET', '/tricks/new');
        $form = $crawler->filter('button[type="submit"]')->form();
        $values = $form->getPhpValues();
        $values['trick']['name'] = 'test1';
        $values['trick']['description'] = 'test1';
        $values['trick']['trickGroup'] = '1';
        $values['trick']['videos'][0]['fullUrl'] = 'https://www.google.com/watch?v=_zNd0jn4CKk';
        $values['trick']['videos'][1]['fullUrl'] = 'google.com';
        $crawler = $client->request('POST', '/tricks/new', $values);

        $this->assertResponseIsSuccessful();
        $this->assertSame(2, $crawler->filter('.form-error-message')->count());
        $this->assertSelectorTextContains('.form-error-message', 'Lien vers youtube invalide.');
    }
    // VIDEO COLLECTION
    // NEW TRICK

    // UPDATE TRICK
    public function testUpdatePageIsUpOnlyWhenLogged()
    {
        $client = static::createClient();
        $trick = static::$container->get(TrickRepository::class)->findOneBy(['name' => 'Dummy trick 0']);

        $client->request('GET', '/tricks/update/' . $trick->getId());
        $this->assertResponseRedirects('/login');

        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $client->request('GET', '/tricks/update/' . $trick->getId());
        $this->assertResponseIsSuccessful();
    }

    public function testSubmitUpdateFormBlankName()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $trick = static::$container->get(TrickRepository::class)->findOneBy(['name' => 'Dummy trick 0']);
        $client->loginUser($user);
        $client->request('GET', '/tricks/update/' . $trick->getId());

        $client->submitForm('Enregistrer', [
            'trick[name]' => '',
            'trick[description]' => 'test',
            'trick[trickGroup]' => 1
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('.form-error-message', 'Veuillez saisir le nom du trick');
    }

    public function testSubmitUpdateFormBlankDescription()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $trick = static::$container->get(TrickRepository::class)->findOneBy(['name' => 'Dummy trick 0']);
        $client->loginUser($user);
        $client->request('GET', '/tricks/update/' . $trick->getId());

        $client->submitForm('Enregistrer', [
            'trick[name]' => 'test',
            'trick[description]' => '',
            'trick[trickGroup]' => 1
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('.form-error-message', 'Veuillez saisir la description du trick');
    }

    public function testSubmitUpdateFormBlankTrickGroup()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $trick = static::$container->get(TrickRepository::class)->findOneBy(['name' => 'Dummy trick 0']);
        $client->loginUser($user);
        $client->request('GET', '/tricks/update/' . $trick->getId());

        $client->submitForm('Enregistrer', [
            'trick[name]' => 'test',
            'trick[description]' => 'test',
            'trick[trickGroup]' => ''
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains(
            '.form-error-message',
            'Veuillez choisir une catégorie pour le trick'
        );
    }

    public function testSubmitUpdateFormValidData()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $trick = static::$container->get(TrickRepository::class)->findOneBy(['name' => 'Dummy trick 0']);
        $client->loginUser($user);
        $client->request('GET', '/tricks/update/' . $trick->getId());

        $client->submitForm('Enregistrer', [
            'trick[name]' => 'test',
            'trick[description]' => 'test',
            'trick[trickGroup]' => 1
        ]);
        $this->assertResponseRedirects();
        $client->followRedirect();
        $this->assertSelectorTextContains('.toast.toast-success', 'Trick modifié !');
    }
    // UPDATE TRICK

    // TRICK DETAIL
    public function testTrickDetailPageIsUpAonimousUser()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/tricks/Air');

        $this->assertResponseIsSuccessful();
        $this->assertSame(2, $crawler->filter('li.photo-li')->count());
        $this->assertSelectorNotExists('.edit-link');
        $this->assertSelectorNotExists('input[name="comment[message]"]');
        $this->assertEquals(5, $crawler->filter('.comment-li')->count());
        $this->assertSelectorExists('#load-more-comments');

        $client->request('GET', '/tricks/Dummy-trick-0');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('.no-more-content', 'Aucun commentaire n\'as été trouvé.');
        $this->assertSelectorNotExists('#load-more-comments');
    }

    /**
     * Logged in user must see update trick link
     * Logged in user must see comment form
     */
    public function testTrickDetailPageIsUpLoggedUser()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $crawler = $client->request('GET', '/tricks/Air');

        $this->assertResponseIsSuccessful();
        $this->assertSame(2, $crawler->filter('li.photo-li')->count());
        $this->assertSelectorExists('.edit-link');
        $this->assertSelectorExists('input[name="comment[message]"]');
        $this->assertEquals(5, $crawler->filter('.comment-li')->count());
        $this->assertSelectorExists('#load-more-comments');
    }

    public function testSubmitCommentValidData()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $client->request('GET', '/tricks/Air');
        $client->submitForm('Laisser un commentaire', [
            'comment[message]' => 'Trop bien !'
        ]);
        $this->assertResponseRedirects('/tricks/Air');
        $client->followRedirect();
        $this->assertSelectorTextContains('.toast.toast-success', 'Commentaire ajouté !');
        $this->assertSelectorTextContains('#comment-holder li:first-of-type', 'Trop bien !');
    }

    public function testSubmitCommentInvalidBlankMessage()
    {
        $client = static::createClient();
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => 'main@snowtricks.com']);
        $client->loginUser($user);
        $client->request('GET', '/tricks/Air');
        $client->submitForm('Laisser un commentaire', [
            'comment[message]' => ''
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('.form-error-message', 'Veuillez saisir un message.');
    }

    public function testDynamicCommentPagination()
    {
        $client = static::createClient();
        $client->request('GET', '/tricks/get-comment-page/1/2');
        $this->assertResponseIsSuccessful();
        $responseContent = json_decode($client->getResponse()->getContent());
        $this->assertContains('<li class="comment-li list-group-item">', $responseContent->html->content);
    }

    public function testDynamicCommentPaginationNoMoreComment()
    {
        $client = static::createClient();
        $client->request('GET', '/tricks/get-comment-page/1/3');
        $this->assertResponseIsSuccessful();
        $responseContent = json_decode($client->getResponse()->getContent());
        $this->assertNotContains('<li class="comment-li">', $responseContent->html->content);
        $this->assertContains('Aucun commentaire n\'as été trouvé.', $responseContent->html->content);
    }
    // TRICK DETAIL

    /**
     * Create a photo for tests
     * @param $photoPath
     * @param $fileCopyName
     * @return UploadedFile
     */
    private function createPhoto($photoPath, $fileCopyName): UploadedFile
    {
        $targetDir = static::$container->getParameter('uploadedPhotoDir');
        $fs = new Filesystem();
        $fs->copy(
            $targetDir . '/' . $photoPath,
            $targetDir . '/' . $fileCopyName,
            true
        );

        return new UploadedFile(
            $targetDir . '/' . $fileCopyName,
            $targetDir . '/' . $photoPath,
            'image/jpeg',
            null,
            true
        );
    }
}
