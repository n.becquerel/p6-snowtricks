<?php

namespace App\Tests\Functional\Controller;

use App\Entity\UserPhoto;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UserProfileControllerTest extends WebTestCase
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\KernelBrowser
     */
    private $client;

    protected function setUp()
    {
        $this->client = static::createClient();
    }

    public function testProfilePageIsUpAndProtected()
    {
        $this->client->request('GET', '/user/profile');
        $this->assertResponseRedirects('/login');
        $this->client->followRedirect();
        $this->assertSelectorTextContains(
            '.toast.toast-danger',
            'Vous devez vous connecter pour accéder à cette page.'
        );

        $this->loginUser('disabled@snowtricks.com');
        $this->client->request('GET', '/user/profile');
        $this->assertResponseRedirects('/');
        $this->client->followRedirect();
        $this->assertSelectorTextContains(
            '.toast.toast-danger',
            'Pour accéder à cette fonctionnalité votre compte doit être activé'
        );

        $this->loginUser('main@snowtricks.com');
        $this->client->request('GET', '/user/profile');
        $this->assertResponseIsSuccessful();
        $this->assertContains('main@snowtricks.com', $this->client->getResponse()->getContent());

        $this->loginUser('alt@snowtricks.com');
        $this->client->request('GET', '/user/profile');
        $this->assertResponseIsSuccessful();
        $this->assertContains('alt@snowtricks.com', $this->client->getResponse()->getContent());
    }

    // EDIT
    public function testEditPageIsUp()
    {
        $this->client->request('GET', '/user/profile/edit');
        $this->assertResponseRedirects('/login');
        $this->client->followRedirect();
        $this->assertSelectorTextContains(
            '.toast.toast-danger',
            'Vous devez vous connecter pour accéder à cette page.'
        );

        $this->loginUser('disabled@snowtricks.com');
        $this->client->request('GET', '/user/profile/edit');
        $this->assertResponseRedirects('/');
        $this->client->followRedirect();
        $this->assertSelectorTextContains(
            '.toast.toast-danger',
            'Pour accéder à cette fonctionnalité votre compte doit être activé'
        );

        $this->loginUser('main@snowtricks.com');
        $this->client->request('GET', '/user/profile/edit');
        $this->assertResponseIsSuccessful();
    }

    public function testEditProfileSubmitFormValidData()
    {
        $this->loginUser('main@snowtricks.com');
        $this->client->enableProfiler();
        $this->client->request('GET', '/user/profile/edit');
        // email does not change so no need to resend validation email
        $this->client->submitForm('Enregistrer', [
            'user_profile[email]' => 'main@snowtricks.com',
            'user_profile[firstname]' => 'Main',
            'user_profile[lastname]' => 'User'
        ]);
        $this->assertEmailCount(0);
        $this->assertResponseRedirects('/user/profile');
        $this->client->followRedirect();
        $this->assertSelectorTextContains('.toast.toast-success', 'Profile modifié !');
    }

    public function testEditProfileSubmitFormBlankEmail()
    {
        $this->loginUser('main@snowtricks.com');
        $this->client->request('GET', '/user/profile/edit');

        $this->client->submitForm('Enregistrer', [
            'user_profile[email]' => '',
            'user_profile[firstname]' => 'Main',
            'user_profile[lastname]' => 'User'
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('.form-error-message', 'Veuillez renseigner une adresse email');
    }

    public function testEditProfileSubmitFormInvalidEmail()
    {
        $this->loginUser('main@snowtricks.com');
        $this->client->request('GET', '/user/profile/edit');

        $this->client->submitForm('Enregistrer', [
            'user_profile[email]' => 'test13213132',
            'user_profile[firstname]' => 'Main',
            'user_profile[lastname]' => 'User'
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains(
            '.form-error-message',
            'Veuillez renseigner une adresse email valide.'
        );
    }

    public function testEditProfileWithPhoto()
    {
        $this->loginUser('main@snowtricks.com');
        $crawler = $this->client->request('GET', '/user/profile/edit');
        $form = $crawler->selectButton('Enregistrer')->form();
        $values = $form->getPhpValues();
        $values['user_profile[email]'] = 'main@snowtricks.com';
        $values['user_profile[firstname]'] = 'main';
        $values['user_profile[lastname]'] = 'user';
        $values['user_profile[photo][file]'] = $this->createUploadedFile('air2.jpeg', 'hello.jpeg');

        $this->client->request($form->getMethod(), $form->getUri(), $values, $form->getPhpFiles());

        $this->assertResponseRedirects('/user/profile');
        $this->client->followRedirect();
        $this->assertSelectorTextContains('.toast.toast-success', 'Profile modifié !');
    }

    public function testDeleteUserPhotoSuccessfull()
    {
        $this->loginUser('main@snowtricks.com');
        $this->client->xmlHttpRequest('POST', '/user/profile/delete-photo/1');
        $this->assertResponseStatusCodeSame(200);
        $photo = $this->client->getContainer()->get('doctrine')->getRepository(UserPhoto::class)->find(1);
        $this->assertNull($photo);
    }
    // EDIT

    private function loginUser(string $email)
    {
        $user = static::$container->get(UserRepository::class)->findOneBy(['email' => $email]);
        $this->client->loginUser($user);
    }

    private function createUploadedFile(string $photoPath, string $fileCopyName): UploadedFile
    {
        $targetDir = static::$container->getParameter('uploadedPhotoDir');
        $fileSys = new Filesystem();
        $fileSys->copy(
            $targetDir . '/' . $photoPath,
            $targetDir . '/' . $fileCopyName,
            true
        );

        return new UploadedFile(
            $targetDir . '/' . $fileCopyName,
            $targetDir . '/' . $photoPath,
            'image/jpeg',
            null,
            true
        );
    }
}
