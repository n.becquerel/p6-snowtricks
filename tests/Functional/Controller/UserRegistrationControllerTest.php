<?php

namespace App\Tests\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserRegistrationControllerTest extends WebTestCase
{
    public function testRegistrationFormPageIsUp()
    {
        $client = self::createClient();
        $client->request('GET', '/signup');
        $this->assertResponseStatusCodeSame(200);
        $this->assertSelectorTextContains('h1', 'Créez votre compte');
    }

    public function testRegistrationSuccessfull()
    {
        $client = self::createClient();
        $client->enableProfiler();
        $client->request('GET', '/signup');
        $client->submitForm('Créer mon compte', [
            'user_registration[email]' => 'test@test.com',
            'user_registration[plainPassword]' => 'password',
            'user_registration[agreeTerms]' =>  true
        ]);
        $this->assertEmailCount(1);
        $this->assertResponseRedirects('/');
        $client->followRedirect();
        $this->assertSelectorTextContains(
            '.toast.toast-success',
            'Votre compte a été créé, veuillez valider votre adresse email'
        );
    }

    public function testRegisterWithExistingEmail()
    {
        $client = self::createClient();
        $client->enableProfiler();
        $client->request('GET', '/signup');
        $client->submitForm('Créer mon compte', [
            'user_registration[email]' => 'main@snowtricks.com',
            'user_registration[plainPassword]' => 'password',
            'user_registration[agreeTerms]' =>  true
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertContains(
            'Un utilisateur possédant déjà cette adresse-email est déjà enregistré.',
            $client->getResponse()->getContent()
        );
    }

    public function testRegistrationBadEmail()
    {
        $client = self::createClient();
        $client->request('GET', '/signup');
        $client->submitForm('Créer mon compte', [
            'user_registration[email]' => 'test@',
            'user_registration[plainPassword]' => 'password',
            'user_registration[agreeTerms]' =>  true
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertContains(
            'L&#039;adresse email est invalide.',
            $client->getResponse()->getContent()
        );
    }

    public function testRegistrationBadPassword()
    {
        $client = self::createClient();
        $client->request('GET', '/signup');
        $client->submitForm('Créer mon compte', [
            'user_registration[email]' => 'test@test.com',
            'user_registration[plainPassword]' => '1',
            'user_registration[agreeTerms]' =>  true
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertContains(
            'Le mot de passe doit faire au moins 8 caractères',
            $client->getResponse()->getContent()
        );
    }

    public function testRegistrationTermsRejected()
    {
        $client = self::createClient();
        $client->request('GET', '/signup');
        $client->submitForm('Créer mon compte', [
            'user_registration[email]' => 'test@test.com',
            'user_registration[plainPassword]' => 'password',
            'user_registration[agreeTerms]' =>  false
        ]);
        $this->assertResponseIsSuccessful();
        $this->assertContains(
            'Vous devez accepter les conditions générales d&#039;utilisation.',
            $client->getResponse()->getContent()
        );
    }

    public function testRegistrationWhenAlreadyLogged()
    {
        $client = self::createClient();
        $client->request('GET', '/login');
        $client->submitForm('Connexion', [
            'email' => 'main@snowtricks.com',
            'password' => 'mainpassword'
        ]);
        $client->followRedirect();
        $client->request('GET', '/signup');
        $this->assertResponseRedirects('/');
        $client->followRedirect();
        $this->assertSelectorTextContains(
            '.toast.toast-danger',
            'Vous semblez déjà posséder un compte. Retour à l\'acceuil.'
        );
    }

    // ACCOUNT VALIDATION
    public function testAccountValidationValidToken()
    {
        $client = static::createClient();
        $client->request('GET', '/validate/account/gBq7hsNbi91OKeT8INSzl811laX6kZQpsPRoQ9x1L8g');
        $this->assertResponseRedirects('/');
        $client->followRedirect();
        $this->assertSelectorTextContains('.toast.toast-success', 'Votre compte est maintenant activé !');
    }

    public function testAccountValidationInvalidToken()
    {
        $client = static::createClient();
        $client->request('GET', '/validate/account/test');
        $this->assertResponseRedirects('/');
        $client->followRedirect();
        $this->assertSelectorTextContains('.toast.toast-danger', 'Impossible d\'activer ce compte.');
    }
}
