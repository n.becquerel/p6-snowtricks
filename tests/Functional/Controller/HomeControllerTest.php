<?php

namespace App\Tests\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    public function testHomePageIsUp()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertResponseStatusCodeSame(200);
        $this->assertCount(8, $crawler->filter('.card-trick'));
    }

    public function testHomePageDynamicPagination()
    {
        $client = static::createClient();
        $client->request('GET', '/get-tricks');
        $this->assertResponseStatusCodeSame(200);
    }
}
