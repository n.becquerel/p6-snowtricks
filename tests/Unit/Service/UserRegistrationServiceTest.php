<?php

namespace App\Tests\Unit\Service;

use App\Entity\User;
use App\Service\UserRegistrationService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class UserRegistrationServiceTest extends TestCase
{
    /**
     * @var UserRegistrationService
     */
    private $service;

    protected function setUp(): void
    {
        $tokenGenerator = $this->getMockBuilder(TokenGeneratorInterface::class)->getMock();
        $tokenGenerator
            ->method('generateToken')
            ->willReturn('hBz0lJj2HgOvLIjHjnB3mjJXb_lyfgOj3UKTAuj5cu0');

        $encoder = $this->getMockBuilder(UserPasswordEncoderInterface::class)->getMock();
        $encoder
            ->method('encodePassword')
            ->willReturn('$2y$13$DEgaMHNXlKS4W93u7IuWtOAhZUVVmSSArq7pbeiRZLYVOvcCMedgW');

        $mailer = $this->getMockBuilder(MailerInterface::class)->getMock();
        $this->service = new UserRegistrationService($tokenGenerator, $encoder, $mailer);
    }

    public function testSetUserForRegistration()
    {
        $user = (new User())->setEmail('test@test.com');
        $plainPassword = 'test';
        $newUser = $this->service->setUserForRegistration($user, $plainPassword);
        $this->assertEquals(
            '$2y$13$DEgaMHNXlKS4W93u7IuWtOAhZUVVmSSArq7pbeiRZLYVOvcCMedgW',
            $newUser->getPassword()
        );
        $this->assertEquals('hBz0lJj2HgOvLIjHjnB3mjJXb_lyfgOj3UKTAuj5cu0', $newUser->getValidationToken());
        $this->assertFalse($newUser->getIsVerified());
    }
}
