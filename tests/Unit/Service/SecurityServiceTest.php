<?php

namespace App\Tests\Unit\Service;

use App\Entity\User;
use App\Service\SecurityService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class SecurityServiceTest extends TestCase
{
    private $tokenGenerator;
    /**
     * @var SecurityService
     */
    private $service;
    /**
     * @var User
     */
    private $user;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $encoder;

    public function setUp()
    {
        $this->tokenGenerator = $this->createMock(TokenGeneratorInterface::class);
        $this->encoder = $this->createMock(UserPasswordEncoderInterface::class);
        $this->service = new SecurityService($this->tokenGenerator, $this->encoder);
        $this->user = new User();
    }

    public function testSetUserForPasswordReset()
    {
        $this->tokenGenerator->method('generateToken')
            ->willReturn('zuQ59ck8RCP7LlZ1JTZvz-2_lYWZJDqW9XvDEb32skg');
        $this->service->setUserForPasswordReset($this->user);
        $this->assertSame('zuQ59ck8RCP7LlZ1JTZvz-2_lYWZJDqW9XvDEb32skg', $this->user->getResetPasswordToken());
    }

    public function testSetNewPassword()
    {
        $this->encoder->method('encodePassword')
            ->willReturn('$2y$13$jHEw5Vk223YspXwZRYWFq.JUHt3EYC9LbhUf6x0lW/TrVdPn4zK6q');
        $this->service->setNewPassword($this->user, 'password');
        $this->assertSame(
            '$2y$13$jHEw5Vk223YspXwZRYWFq.JUHt3EYC9LbhUf6x0lW/TrVdPn4zK6q',
            $this->user->getPassword()
        );
    }
}
