<?php

namespace App\Tests\Unit\EventListener;

use App\Entity\Trick;
use App\Entity\TrickGroup;
use App\Entity\User;
use App\EventListener\TrickListener;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\AsciiSlugger;

class TrickListenerTest extends TestCase
{
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $slugger;
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    private $security;

    protected function setUp()
    {
        parent::setUp();
        $this->slugger = new AsciiSlugger();
        $this->security = $this->createMock(Security::class);
    }

    public function testTrickPrePersist()
    {
        $trick = (new Trick())
            ->setName('Test')
            ->setDescription('test')
            ->setTrickGroup((new TrickGroup())->setLabel('test'));
        $user = new User();
        $this->security->method('getUser')->willReturn($user);
        $listener = new TrickListener($this->security, $this->slugger);

        $listener->prePersist($trick);

        $this->assertSame('Test', $trick->getSlug());
        $this->assertSame($user, $trick->getCreatedBy());
        $this->assertInstanceOf(DateTimeInterface::class, $trick->getCreatedAt());
    }

    public function testTrickPostUpdate()
    {
        $trick = (new Trick())
            ->setName('Test')
            ->setDescription('test')
            ->setTrickGroup((new TrickGroup())->setLabel('test'));
        $user = new User();
        $this->security->method('getUser')->willReturn($user);
        $listener = new TrickListener($this->security, $this->slugger);

        $listener->preUpdate($trick);

        $this->assertSame('Test', $trick->getSlug());
        $this->assertSame($user, $trick->getUpdatedBy());
        $this->assertInstanceOf(DateTimeInterface::class, $trick->getUpdatedAt());
    }
}
