<?php

namespace App\Tests\Unit\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testSetUserVerified()
    {
        $user = (new User())->setUserVerified();
        $this->assertTrue($user->getIsVerified());
        $this->assertNull($user->getValidationToken());
    }
}
