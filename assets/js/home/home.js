import '../../styles/home/home.scss';

$(document).ready(function () {

    $('#load-more').on('click', function () {
        let $button = $(this);
        let $loader = $('#spinner-loader');
        $loader.removeClass('hidden');
        let index = parseInt($button.attr('data-index'), 10);

        $.get(`/get-tricks/${index + 1}`)
            .then((response) => {
                if (typeof response.html !== 'undefined') {
                    $button.attr('data-index', index + 1);
                    $loader.addClass('hidden');
                    $('#tricks-container').append(response.html.content);
                }
            });
    });

});
