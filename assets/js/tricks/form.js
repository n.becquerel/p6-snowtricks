import '../../styles/tricks/tricks-form.scss';

import {youtubeParser, baseIframe} from '../services/trick-service';

$(document).ready(function () {

    // base iframe
    const videoIframe = baseIframe;

    let $submitButton = $('#submit-trick-form');

    const $photoCollectionHolder = $('#photos-collection');
    const $videoCollectionHolder = $('#videos-collection');

    // toggle iframe or error message
    function updateVideoVisibility($this)
    {
        const input = $this;

        input.closest('li').children('iframe').remove();
        input.siblings('.invalid-feedback').remove();

        const id = youtubeParser(input.val());

        if (id) {
            $submitButton.removeAttr('disabled');
            input.removeClass('is-invalid');
            input.closest('li').prepend(videoIframe.replace(/__id__/gi, id));
        } else {
            $submitButton.attr('disabled', 'disabled');
            input.addClass('is-invalid');
            input.closest('.form-group').prepend(
                '<span class="invalid-feedback d-block">' +
                '<span class="d-block">' +
                '<span class="form-error-icon badge badge-danger text-uppercase">Error</span>' +
                '<span class="form-error-message">Le lien n\'est pas reconnu en tant que lien valide.</span>' +
                '</span>' +
                '</span>'
            );
        }
    }

    function addTrickPhotoField()
    {
        let prototype = $photoCollectionHolder.attr('data-prototype');
        let index = parseInt($photoCollectionHolder.attr('data-index'), 10);

        const newForm = prototype.replaceAll(/__name__/gi, index);

        $photoCollectionHolder.append('<li class="photo-li col-12 col-lg-3 mb-3">' + newForm + '</li>');
        $photoCollectionHolder.attr('data-index', index + 1);

        // focus the new input form
        $(`#trick_photos_${index}_file`).click();
    }

    function removePhotoTrickField()
    {
        const fieldIndex = parseInt($(this).attr('data-index'), 10);

        const $field = $photoCollectionHolder.find(`#trick_photos_${fieldIndex}`).closest('li');

        $field.fadeOut(400, () => {
            $field.remove();
        });
    }

    function addTrickVideoField()
    {
        let prototype = $videoCollectionHolder.attr('data-prototype');
        let index = parseInt($videoCollectionHolder.attr('data-index'), 10);

        const newForm = prototype.replaceAll(/__name__/gi, index);

        $videoCollectionHolder.append('<li class="video-li col-12 col-lg-3 mb-3">' + newForm + '</li>');
        $videoCollectionHolder.attr('data-index', index + 1);
    }

    function removeVideoTrickField()
    {
        const fieldIndex = parseInt($(this).attr('data-index'), 10);
        const $field = $videoCollectionHolder.find(`#trick_videos_${fieldIndex}`).closest('li');
        $field.fadeOut(400, () => {
            $field.remove();
        });
    }

    $('#add-photo-form').on('click', addTrickPhotoField);

    $(document).on('click', '.remove-photo-button', removePhotoTrickField);

    $('#add-video-form').on('click', addTrickVideoField);

    $(document).on('click', '.remove-video-button', removeVideoTrickField);

    // toggle filename in input + show photo preview
    $(document).on('change', '.custom-file-input', function (event) {
        const $this = $(this);
        // toggle filename
        let fileName = $this.val().replace('C:\\fakepath\\', '');
        $this.next('.custom-file-label').html(fileName);

        // show preview
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onload = () => {
            const $holder = $this.closest('.form-group');
            const $img = $('<img alt="uploaded file" src="" class="img-fluid" style="object-fit: cover" height="150"/>')
                .attr('src', reader.result);
            $holder.find('img').remove();
            $holder.prepend($img);
        };
        reader.readAsDataURL(file);
    });

    // toggle iframe on input change
    $(document).on('change', '.video-link-input', function () {
        try {
            updateVideoVisibility($(this));
        } catch (error) {
            $submitButton.attr('disabled', 'disabled');
        }
    });

    // toggle iframe on update trick page
    $('.video-link-input').each((index, input) => {
        updateVideoVisibility($(input));
    });
});
