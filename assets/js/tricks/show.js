import '../../styles/tricks/tricks-show.scss';

import {youtubeParser, baseIframe} from '../services/trick-service';

$(document).ready(function () {

    $('.video-link').each((index, item) => {
        const iframe = baseIframe.replace(/__id__/gi, youtubeParser($(item).val()));
        $(item).closest('.video-li').prepend(iframe);
    });

    const $commentHolder = $('#comment-holder');
    const trickId = $('#trick-id').val();
    const $spinnerLoader = $('#spinner-loader');

    $(document).on('click', '#load-more-comments', function () {
        const $loadMoreButton = $(this);
        const page = parseInt($loadMoreButton.attr('data-index'), 10);
        $spinnerLoader.removeClass('hidden');
        $.get(`/tricks/get-comment-page/${trickId}/${page + 1}`, function (response) {
            if (typeof response.html.content !== 'undefined') {
                $('.no-more-content').remove();
                $commentHolder.append(response.html.content);
                if (!response.html.content.includes('Aucun commentaire n\'as été trouvé.')) {
                    $loadMoreButton.attr('data-index', page + 1);
                }
            }
            $spinnerLoader.addClass('hidden');
        });
    });

});
