import '../../styles/user_registration/signup.scss';

function toggleAnimation($input)
{
    $input.parents('.form-group').addClass('focused');
}

function toggleViewMode($input)
{
    const inputValue = $input.val();
    if (inputValue === '') {
        $input.removeClass('filled');
        $input.parents('.form-group').removeClass('focused');
    } else {
        $input.addClass('filled');
    }
}

$(document).ready(function () {
    const $inputs = $('input');

    $inputs.each(function (index, item) {
        toggleAnimation($(item));
        toggleViewMode($(item));
    });

    $inputs.focus(function () {
        toggleAnimation($(this));
    });

    $inputs.keydown(function () {
        $inputs.each(function (index, item) {
            $(item).removeClass('is-invalid');
        });
    });

    $inputs.blur(function () {
        toggleViewMode($(this));
    });

});
