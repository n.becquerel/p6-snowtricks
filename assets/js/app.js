import '../styles/app.scss';

import $ from 'jquery';
// import Popper from "popper.js";
import 'bootstrap/dist/js/bootstrap';

$(document).ready(function () {
    $('body').tooltip({selector: '[data-toggle="tooltip"]'});
    $('.toast').toast({ animation: true, autohide: true, delay: 5000 });
    $('.toast').toast('show');
});