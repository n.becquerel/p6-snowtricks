import '../../styles/user_profile/form.scss';

import Swal from 'sweetalert2';

$(document).ready(function () {

    $('.btn-delete-photo').click(function () {
        const id = $(this).attr('data-index');
        Swal.fire({
            title: 'Supprimer ?',
            text: 'Êtes-vous sûr(e) de vouloir supprimer votre photo de profil ?\r\nCette action est irréversible.',
            icon: 'warning',
            confirmButtonText: 'Supprimer',
            cancelButtonText: 'Annuler',
            showCancelButton: true
        }).then((response) => {
            if (response.value) {
                $.post(`/user/profile/delete-photo/${id}`, {}, function (response) {
                    if (typeof response !== 'undefined') {
                        if (response.success === true) {
                            $('.profile-photo').attr('src', '/uploadedPhotos/user-photo-placeholder.png');
                        }
                    }
                });
            }
        });
    });

    // toggle filename in input + show photo preview
    $(document).on('change', '.custom-file-input', function (event) {
        const $this = $(this);
        // toggle filename
        let fileName = $this.val().replace('C:\\fakepath\\', '');
        $this.next('.custom-file-label').html(fileName);

        // show preview
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.onload = () => {
            const $holder = $this.closest('.form-group');
            const $element = $('<div class="text-center my-3 my-lg-4">');
            const $img = $('<img alt="uploaded file" src="" class="profile-photo rounded-circle"/>').attr('src', reader.result);
            $holder.prepend($element.append($img));
        };
        reader.readAsDataURL(file);
    });

});
