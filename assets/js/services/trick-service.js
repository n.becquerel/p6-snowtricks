function youtubeParser(url)
{
    const regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
    const match = url.match(regExp);
    return (match&&match[7].length===11)? match[7] : false;
}

const baseIframe = '<iframe width="560" height="315" src="https://www.youtube.com/embed/__id__" class="border-0" ' +
    'allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" ' +
    'allowfullscreen style="width: 100% !important; height: auto !important;"></iframe>';

module.exports = {
    youtubeParser,
    baseIframe
};
