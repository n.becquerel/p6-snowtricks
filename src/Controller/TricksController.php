<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Trick;
use App\Form\CommentType;
use App\Form\Handler\CommentHandler;
use App\Form\Handler\TricksHandler;
use App\Form\Maker\FormMaker;
use App\Form\TrickType;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tricks", name="tricks_")
 * Class TricksController
 * @package App\Controller
 */
class TricksController extends AbstractController
{
    /**
     * @Route("/new", name="new", methods={"GET", "POST"})
     * @param Request $request
     * @param FormMaker $formMaker
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function new(Request $request, FormMaker $formMaker)
    {
        $trick = new Trick();
        $form = $formMaker->makeForm(TrickType::class, $trick);

        if ($formMaker->handle($request, TricksHandler::class)) {
            $this->addFlash('success', 'Trick crée !');
            return $this->redirectToRoute('tricks_update', ['id' => $trick->getId()]);
        }

        return $this->render('tricks/form.html.twig', ['form' => $form->createView(), 'trick' => $trick]);
    }

    /**
     * @Route("/update/{id}", name="update", methods={"GET", "POST"})
     * @param Request $request
     * @param Trick $trick
     * @param TricksHandler $tricksHandler
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, Trick $trick, FormMaker $formMaker)
    {
        $form = $formMaker->makeForm(TrickType::class, $trick);

        if ($formMaker->handle($request, TricksHandler::class)) {
            $this->addFlash('success', 'Trick modifié !');
            return $this->redirectToRoute('tricks_update', ['id' => $trick->getId()]);
        }

        return $this->render('tricks/form.html.twig', ['form' => $form->createView(), 'trick' => $trick]);
    }

    /**
     * @Route("/{slug}", name="show", methods={"GET", "POST"})
     * @param Trick $trick
     * @param CommentRepository $commentRepository
     * @param integer $commentsPerPages
     * @param FormMaker $formMaker
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(
        Trick $trick,
        CommentRepository $commentRepository,
        int $commentsPerPages,
        FormMaker $formMaker,
        Request $request
    ) {
        $comment = new Comment();

        $comments = $commentRepository->getList($trick, 1, $commentsPerPages);

        $commentForm = $formMaker->makeForm(CommentType::class, $comment);

        if ($formMaker->handle($request, CommentHandler::class)) {
            $this->addFlash('success', 'Commentaire ajouté !');
            return $this->redirectToRoute('tricks_show', ['slug' => $trick->getSlug()]);
        }

        return $this->render(
            'tricks/show.html.twig',
            [
                'trick' => $trick, 'commentForm' => $commentForm->createView(),
                'comments' => $comments,
                'commentsPerPages' => $commentsPerPages
            ]
        );
    }

    /**
     * @Route(
     *     "/get-comment-page/{trick}/{page}",
     *     name="get_comment_page",
     *     methods={"GET"},
     *     defaults={"page"=2}
     * )
     * @param Trick $trick
     * @param int $page
     * @param CommentRepository $commentRepository
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getCommentPage(Trick $trick, int $page, CommentRepository $commentRepository)
    {
        $comments = $commentRepository->getList($trick, $page);

        return $this->json(
            [
                'html' => $this->render('tricks/include/_comment-list.html.twig', ['comments' => $comments])
            ]
        );
    }
}
