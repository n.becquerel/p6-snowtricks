<?php

namespace App\Controller;

use App\Dto\UserRegistrationInput;
use App\Form\Handler\UserRegistrationHandler;
use App\Form\Maker\FormMaker;
use App\Form\UserRegistrationType;
use App\Repository\UserRepository;
use App\Service\UserRegistrationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserRegistrationController extends AbstractController
{
    /**
     * @Route("/signup", name="user_registration")
     * @param Request $request
     * @param UserRegistrationService $registrationService
     * @param MailerInterface $mailer
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function index(Request $request, FormMaker $formMaker)
    {
        if ($this->getUser() !== null) {
            $this->addFlash('danger', 'Vous semblez déjà posséder un compte. Retour à l\'acceuil.');
            return $this->redirectToRoute('home');
        }

        $userRegistrationInput = new UserRegistrationInput();
        $form = $formMaker->makeForm(UserRegistrationType::class, $userRegistrationInput);

        if ($formMaker->handle($request, UserRegistrationHandler::class)) {
            $this->addFlash('success', 'Votre compte a été créé, veuillez valider votre adresse email');
            return $this->redirectToRoute('home');
        }

        return $this->render('user_registration/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/validate/account/{token}", name="registration_validation")
     * @param string $token
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function accountValidation(string $token, UserRepository $userRepository, EntityManagerInterface $em)
    {
        $user = $userRepository->findOneBy(['validationToken' => $token]);

        if (!$user) {
            $this->addFlash('danger', 'Impossible d\'activer ce compte.');
        } else {
            $user->setUserVerified();
            $em->flush();

            $this->addFlash('success', 'Votre compte est maintenant activé !');
        }

        return $this->redirectToRoute('home');
    }
}
