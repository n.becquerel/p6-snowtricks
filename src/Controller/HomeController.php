<?php

namespace App\Controller;

use App\Entity\Trick;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $tricks = $this->getDoctrine()->getRepository(Trick::class)->getList();

        return $this->render('home/index.html.twig', [
            'tricks' => $tricks,
        ]);
    }

    /**
     * @Route(
     *     "/get-tricks/{page}",
     *     name="home_get_trick_page",
     *     methods={"GET"},
     *     defaults={"page"=2}
     * )
     * @param int $page
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getTrickPage(int $page)
    {
        $tricks = $this->getDoctrine()->getRepository(Trick::class)->getList($page);

        return $this->json(['html' => $this->render('home/include/_trick-list.html.twig', ['tricks' => $tricks])]);
    }
}
