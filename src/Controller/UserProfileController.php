<?php

namespace App\Controller;

use App\Dto\UserProfileInput;
use App\Entity\User;
use App\Entity\UserPhoto;
use App\Form\Handler\UserProfileHandler;
use App\Form\Maker\FormMaker;
use App\Form\UserProfileType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user/profile", name="user_profile_")
 * Class UserProfileController
 * @package App\Controller
 */
class UserProfileController extends AbstractController
{
    /**
     * @Route("", name="show")
     */
    public function index()
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user->getIsVerified()) {
            throw $this->createAccessDeniedException();
        }

        return $this->render('user_profile/show.html.twig', ['user' => $this->getUser()]);
    }

    /**
     * @Route("/edit", name="edit", methods={"GET", "POST"})
     * @param FormMaker $formMaker
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(Request $request, FormMaker $formMaker)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user->getIsVerified()) {
            throw $this->createAccessDeniedException();
        }

        $userProfileInput = new UserProfileInput($user);

        $form = $formMaker->makeForm(UserProfileType::class, $userProfileInput);

        if ($formMaker->handle($request, UserProfileHandler::class)) {
            $this->addFlash('success', 'Profile modifié !');
            return $this->redirectToRoute('user_profile_show');
        }

        return $this->render('user_profile/form.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/delete-photo/{id}", name="delete_photo", methods={"POST"}, condition="request.isXmlHttpRequest()")
     * @param UserPhoto $photo
     * @param EntityManagerInterface $manager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteUserPhoto(UserPhoto $photo, EntityManagerInterface $manager)
    {
        $manager->remove($photo);
        $manager->flush();

        return $this->json(['success' => true], 200);
    }
}
