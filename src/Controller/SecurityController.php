<?php

namespace App\Controller;

use App\Dto\Security\ForgotPasswordInput;
use App\Dto\Security\ResetPasswordInput;
use App\Form\Handler\RequestResetPasswordHandler;
use App\Form\Handler\ResetPasswordHandler;
use App\Form\Maker\FormMaker;
use App\Form\Security\ForgotPasswordType;
use App\Form\Security\ResetPasswordType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
    }

    /**
     * @Route("/forgot-password", name="forgot_password", methods={"GET", "POST"})
     * @param Request $request
     * @param FormMaker $formMaker
     * @return Response
     */
    public function requestNewPassword(
        Request $request,
        FormMaker $formMaker
    ) {
        $forgotPasswordInput = new ForgotPasswordInput();
        $form = $formMaker->makeForm(ForgotPasswordType::class, $forgotPasswordInput);

        if ($formMaker->handle($request, RequestResetPasswordHandler::class)) {
            $this->addFlash(
                'success',
                'Si un compte est associé à l\'adresse email saisie, vous allez recevoir un email contenant' .
                ' les instructions à suivre.'
            );
            return $this->redirectToRoute('home');
        }

        return $this->render('security/forgot-password.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/reset-password/{token}", name="reset_password", methods={"POST", "GET"})
     * @param string $token
     * @param Request $request
     * @param UserRepository $userRepository
     * @param FormMaker $formMaker
     * @return Response
     */
    public function resetPassword(
        string $token,
        Request $request,
        UserRepository $userRepository,
        FormMaker $formMaker
    ) {
        $user = $userRepository->findOneBy(['resetPasswordToken' => $token]);

        if (!$user) {
            $this->addFlash('danger', 'Impossible de changer le mot de passe de ce compte.');
            return $this->redirectToRoute('home');
        }

        $resetPasswordInput = new ResetPasswordInput();
        $form = $formMaker->makeForm(ResetPasswordType::class, $resetPasswordInput);

        if ($formMaker->handle($request, ResetPasswordHandler::class)) {
            $this->addFlash('success', 'Mot de passe modifié.');
            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/reset-password.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
