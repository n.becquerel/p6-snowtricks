<?php

namespace App\Dto;

use App\Validator\Constraint\UniqueMembership;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class UserRegistrationInput
 * @package App\Dto
 */
class UserRegistrationInput
{
    /**
     * @var string
     * @Assert\NotBlank(message="L'adresse email est obligatoire.")
     * @Assert\Email(message="L'adresse email est invalide.")
     * @UniqueMembership()
     */
    public $email;
    /**
     * @var string
     * @Assert\NotBlank(message="Le mot de passe est obligatoire")
     * @Assert\Length(min="8", minMessage="Le mot de passe doit faire au moins 8 caractères.")
     */
    public $plainPassword;
    /**
     * @var bool
     * @Assert\IsTrue(message="Vous devez accepter les conditions générales d'utilisation.")
     */
    public $agreeTerms;
}
