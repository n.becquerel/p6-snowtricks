<?php

namespace App\Dto\Security;

use Symfony\Component\Validator\Constraints as Assert;

class ResetPasswordInput
{
    /**
     * @var string
     * @Assert\NotBlank(message="Veuillez saisir un nouveau mot de passe.")
     * @Assert\Length(min="8", minMessage="Le mot de passe doit faire au moins 8 caractères.")
     */
    public $plainPassword;
}
