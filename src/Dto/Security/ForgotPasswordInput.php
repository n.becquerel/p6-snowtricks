<?php

namespace App\Dto\Security;

use Symfony\Component\Validator\Constraints as Assert;

class ForgotPasswordInput
{
    /**
     * @var string
     * @Assert\Email(message="Adresse email invalide.")
     */
    public $email;
}
