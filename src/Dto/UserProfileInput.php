<?php

namespace App\Dto;

use App\Entity\User;
use App\Entity\UserPhoto;
use Symfony\Component\Validator\Constraints as Assert;

class UserProfileInput
{
    /**
     * @var string
     * @Assert\NotBlank(message="Veuillez renseigner une adresse email.")
     * @Assert\Email(message="Veuillez renseigner une adresse email valide.")
     */
    public $email;

    /**
     * @var string
     */
    public $firstname;

    /**
     * @var string
     */
    public $lastname;

    /**
     * @var UserPhoto|null
     */
    public $photo = null;

    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->email = $user->getEmail();
        $this->firstname = $user->getFirstname();
        $this->lastname = $user->getLastname();
        $this->photo = $user->getPhoto();
    }

    public function updateUser(): void
    {
        $this->user->setEmail($this->email);
        $this->user->setLastname($this->lastname);
        $this->user->setFirstname($this->firstname);
        $this->user->setPhoto($this->photo);
    }
}
