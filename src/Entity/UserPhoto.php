<?php

namespace App\Entity;

use App\Repository\UserPhotoRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserPhotoRepository::class)
 */
class UserPhoto extends TrickPhoto
{
    /**
     * @ORM\OneToOne(targetEntity=User::class, inversedBy="photo")
     */
    private $user;

    public function __construct()
    {
        $this->setFilename(null);
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
