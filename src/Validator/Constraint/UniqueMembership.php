<?php

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UniqueMembership extends Constraint
{
    public $message = 'Un utilisateur possédant déjà cette adresse-email est déjà enregistré.';

    public function validatedBy()
    {
        return UniqueMembershipValidator::class;
    }
}
