<?php

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * Class UniqueTrick
 * @package App\Validator\Constraint
 */
class UniqueTrick extends Constraint
{
    public $message = 'Un trick possédant le même nom existe déjà. Veuillez choisir un autre nom.';

    public function validatedBy()
    {
        return UniqueTrickValidator::class;
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
