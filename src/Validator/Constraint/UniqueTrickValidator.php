<?php

namespace App\Validator\Constraint;

use App\Repository\TrickRepository;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueTrickValidator extends ConstraintValidator
{
    /**
     * @var TrickRepository
     */
    private $trickRepository;
    /**
     * @var SluggerInterface
     */
    private $slugger;

    public function __construct(TrickRepository $trickRepository, SluggerInterface $slugger)
    {
        $this->trickRepository = $trickRepository;
        $this->slugger = $slugger;
    }

    /**
     * @inheritDoc
     */
    public function validate($trick, Constraint $constraint)
    {
        if ($trick->getId()) {
            return;
        }

        if (!$trick->getName()) {
            return;
        }

        $trick = $this->trickRepository->findOneBy(['slug' => $this->slugger->slug($trick->getName())]);

        if ($trick) {
            $this->context->buildViolation($constraint->message)->atPath('name')->addViolation();
        }
    }
}
