<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class UserRegistrationService
{
    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var MailerInterface
     */
    private $mailer;

    public function __construct(
        TokenGeneratorInterface $tokenGenerator,
        UserPasswordEncoderInterface $encoder,
        MailerInterface $mailer
    ) {
        $this->tokenGenerator = $tokenGenerator;
        $this->encoder = $encoder;
        $this->mailer = $mailer;
    }

    public function setUserForRegistration(User $user, string $plainPassword): User
    {
        return $user->setPassword($this->encoder->encodePassword($user, $plainPassword))
            ->setValidationToken($this->tokenGenerator->generateToken())
            ->setIsVerified(false);
    }
}
