<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploaderService
{
    /**
     * @var string
     */
    private $targetDir;
    /**
     * @var SluggerInterface
     */
    private $slugger;

    public function __construct(string $targetDir, SluggerInterface $slugger)
    {
        $this->targetDir = $targetDir;
        $this->slugger = $slugger;
    }

    public function upload(UploadedFile $file)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

//        try {
            $file->move($this->getTargetDirectory(), $fileName);
//        } catch (FileException $exception) {
//
//        }

        return $fileName;
    }

    private function getTargetDirectory()
    {
        return $this->targetDir;
    }
}
