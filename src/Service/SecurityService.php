<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class SecurityService
{
    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * SecurityService constructor.
     * @param TokenGeneratorInterface $tokenGenerator
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(TokenGeneratorInterface $tokenGenerator, UserPasswordEncoderInterface $encoder)
    {
        $this->tokenGenerator = $tokenGenerator;
        $this->encoder = $encoder;
    }

    public function setUserForPasswordReset(User $user)
    {
        $user->setResetPasswordToken($this->tokenGenerator->generateToken());
    }

    public function setNewPassword(User $user, string $plainPassword)
    {
        $user->setPassword($this->encoder->encodePassword($user, $plainPassword))->setResetPasswordToken(null);
    }
}
