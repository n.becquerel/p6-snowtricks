<?php

namespace App\Repository;

use App\Entity\Trick;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Trick|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trick|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trick[]    findAll()
 * @method Trick[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrickRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trick::class);
    }

    public function getList(int $page = 1, int $itemsPerPage = 8): Paginator
    {
        $query = $this->createQueryBuilder('t')
            ->orderBy('t.createdAt');

        $paginator = new Paginator($query, true);
        $paginator->getQuery()->setMaxResults($itemsPerPage)->setFirstResult($itemsPerPage * ($page - 1));

        return $paginator;
    }
}
