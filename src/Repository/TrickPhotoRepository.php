<?php

namespace App\Repository;

use App\Entity\TrickPhoto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TrickPhoto|null find($id, $lockMode = null, $lockVersion = null)
 * @method TrickPhoto|null findOneBy(array $criteria, array $orderBy = null)
 * @method TrickPhoto[]    findAll()
 * @method TrickPhoto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TrickPhotoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TrickPhoto::class);
    }
}
