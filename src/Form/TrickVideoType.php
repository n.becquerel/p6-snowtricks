<?php

namespace App\Form;

use App\Entity\TrickVideo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrickVideoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullUrl', UrlType::class, [
                'default_protocol' => 'https',
                'attr' => [
                    'placeholder' => 'Insérez l\'url d\'une vidéo youtube.',
                    'class' => 'video-link-input'
                ],
                'label' => false
            ])
            ->add('removalButton', ButtonType::class, [
                'label' => '<i class="fa fa-trash"></i>',
                'label_html' => true,
                'attr' => [
                    'id' => 'remove-video-button',
                    'data-index' => '__name__',
                    'class' => 'btn-danger remove-video-button rounded-0 btn-sm ml-auto',
                    'style' => 'position: relative; display: inherit'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TrickVideo::class,
        ]);
    }
}
