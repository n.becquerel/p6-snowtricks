<?php

namespace App\Form;

use App\Entity\TrickPhoto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class TrickPhotoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, [
                'required' => false,
                'label' => false,
                'constraints' => [
                    new File([
                        'uploadIniSizeErrorMessage' => 'Le fichier ne peut pas dépasser {{ limit }} {{ suffix }}.',
                        'mimeTypes' => ['image/*'],
                        'mimeTypesMessage' => 'Le fichier doit être une image (ex: jpeg, png etc...)',
                    ])
                ]
            ])
            ->add('removalButton', ButtonType::class, [
                'label' => '<i class="fa fa-trash"></i>',
                'label_html' => true,
                'attr' => [
                    'id' => 'remove-photo-field',
                    'data-index' => '__name__',
                    'class' => 'btn-danger remove-photo-button rounded-0 btn-sm ml-auto',
                    'style' => 'position: relative; display: inherit'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TrickPhoto::class,
        ]);
    }
}
