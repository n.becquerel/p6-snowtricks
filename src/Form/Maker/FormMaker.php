<?php

namespace App\Form\Maker;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class FormMaker
{
    /**
     * @var FormInterface|null
     */
    private $form;
    /**
     * @var ContainerInterface
     */
    private $container;
    private $entity;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function makeForm(string $formType, $entity, array $formOptions = [])
    {
        $this->entity = $entity;

        $this->form = $this->container->get('form.factory')->create($formType, $entity, $formOptions);

        return $this->form;
    }

    public function handle(Request $request, string $handlerClassName, string $managerName = 'default')
    {
        $this->form->handleRequest($request);

        if ($this->checkIfFormIsValid()) {
            $this->handleSucess($request, $this->entity, $handlerClassName, $managerName);
            return true;
        }

        return null;
    }

    private function checkIfFormIsValid(): bool
    {
        return $this->form->isSubmitted() && $this->form->isValid();
    }

    private function handleSucess(Request $request, $entity, string $handlerClassName, string $managerName)
    {
        return $this->container->get($handlerClassName)->withRequest($request)->handle($entity, $managerName);
    }
}
