<?php

namespace App\Form\Handler;

use App\Entity\User;
use App\Service\UserRegistrationService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class UserRegistrationHandler extends AbstractHandler
{
    /**
     * @var UserRegistrationService
     */
    private $registrationService;
    /**
     * @var MailerInterface
     */
    private $mailer;
    /**
     * @var string
     */
    private $doNotAnswerEmail;

    public function __construct(
        ManagerRegistry $doctrine,
        UserRegistrationService $registrationService,
        MailerInterface $mailer,
        string $doNotAnswerEmail
    ) {
        parent::__construct($doctrine);
        $this->registrationService = $registrationService;
        $this->mailer = $mailer;
        $this->doNotAnswerEmail = $doNotAnswerEmail;
    }

    public function handle($entity, string $managerName)
    {
        $user = (new User())->setEmail($entity->email);
        $user = $this->registrationService->setUserForRegistration($user, $entity->plainPassword);
        $manager = $this->doctrine->getManager($managerName);
        $manager->persist($user);
        $manager->flush();

        $email = (new TemplatedEmail())
            ->from($this->doNotAnswerEmail)
            ->to($user->getEmail())
            ->htmlTemplate('mail/registration/welcome.html.twig')
            ->context(['token' => $user->getValidationToken()])
            ->subject('Bienvenue sur Snow Tricks !');

        $this->mailer->send($email);
    }
}
