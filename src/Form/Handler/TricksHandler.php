<?php

namespace App\Form\Handler;

class TricksHandler extends AbstractHandler
{
    public function handle($entity, string $managerName)
    {
        if (!$entity->getId()) {
            return $this->handleNew($entity, $managerName);
        }

        return $this->handleEdit($managerName);
    }

    private function handleNew($entity, string $managerName)
    {
        $manager = $this->doctrine->getManager($managerName);
        $manager->persist($entity);
        $manager->flush();
    }

    private function handleEdit(string $managerName)
    {
        $this->doctrine->getManager($managerName)->flush();
    }
}
