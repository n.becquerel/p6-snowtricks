<?php

namespace App\Form\Handler;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractHandler
{
    /**
     * @var ManagerRegistry
     */
    protected $doctrine;
    protected $entity;
    /**
     * @var Request
     */
    protected $request;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    abstract public function handle($entity, string $managerName);

    /**
     * Store Request
     * @param Request $request
     * @return $this
     */
    public function withRequest(Request $request): self
    {
        $this->request = $request;
        return $this;
    }
}
