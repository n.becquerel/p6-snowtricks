<?php

namespace App\Form\Handler;

use App\Repository\UserRepository;
use App\Service\SecurityService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;

class RequestResetPasswordHandler extends AbstractHandler
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var SecurityService
     */
    private $securityService;
    /**
     * @var MailerInterface
     */
    private $mailer;
    /**
     * @var string
     */
    private $doNotAnswerEmail;

    public function __construct(
        ManagerRegistry $doctrine,
        UserRepository $userRepository,
        SecurityService $securityService,
        MailerInterface $mailer,
        string $doNotAnswerEmail
    ) {
        parent::__construct($doctrine);
        $this->userRepository = $userRepository;
        $this->securityService = $securityService;
        $this->mailer = $mailer;
        $this->doNotAnswerEmail = $doNotAnswerEmail;
    }

    public function handle($entity, string $managerName)
    {
        $user = $this->userRepository->findOneBy(['email' => $entity->email]);

        $manager = $this->doctrine->getManager($managerName);

        if ($user) {
            $this->securityService->setUserForPasswordReset($user);
            $manager->flush();

            $email = (new TemplatedEmail())
                ->from($this->doNotAnswerEmail)
                ->to($user->getEmail())
                ->htmlTemplate('mail/security/reset-password.html.twig')
                ->context(['token' => $user->getResetPasswordToken()])
                ->subject('Réinitialisation de mot de passe');

            $this->mailer->send($email);
        }
    }
}
