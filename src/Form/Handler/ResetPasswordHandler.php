<?php

namespace App\Form\Handler;

use App\Repository\UserRepository;
use App\Service\SecurityService;
use Doctrine\Persistence\ManagerRegistry;

class ResetPasswordHandler extends AbstractHandler
{
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var SecurityService
     */
    private $securityService;

    public function __construct(
        ManagerRegistry $doctrine,
        UserRepository $userRepository,
        SecurityService $securityService
    ) {
        parent::__construct($doctrine);
        $this->userRepository = $userRepository;
        $this->securityService = $securityService;
    }

    public function handle($entity, string $managerName)
    {
        $token = $this->request->get('token');
        $user = $this->userRepository->findOneBy(['resetPasswordToken' => $token]);
        $manager = $this->doctrine->getManager($managerName);
        $this->securityService->setNewPassword($user, $entity->plainPassword);
        $manager->flush();
    }
}
