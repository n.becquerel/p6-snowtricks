<?php

namespace App\Form\Handler;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

class CommentHandler extends AbstractHandler
{
    /**
     * @var \Symfony\Component\Security\Core\User\UserInterface|null
     */
    private $user;

    public function __construct(
        ManagerRegistry $doctrine,
        Security $security
    ) {
        parent::__construct($doctrine);
        $this->user = $security->getUser();
    }

    public function handle($entity, string $managerName)
    {
        return $this->handleNew($entity, $managerName);
    }

    private function handleNew($entity, string $managerName)
    {
        $trick = $this->request->attributes->get('trick');

        $entity->setCreatedBy($this->user)->setTrick($trick);

        $manager = $this->doctrine->getManager($managerName);
        $manager->persist($entity);
        $manager->flush();
    }
}
