<?php

namespace App\Form\Handler;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class UserProfileHandler extends AbstractHandler
{
    /**
     * @var UserInterface|null
     */
    private $user;

    public function __construct(
        ManagerRegistry $doctrine,
        Security $security
    ) {
        parent::__construct($doctrine);
        $this->user = $security->getUser();
    }

    public function handle($entity, string $managerName)
    {
        return $this->edit($entity, $managerName);
    }

    private function edit($entity, string $managerName)
    {
        $entity->updateUser();

        $manager = $this->doctrine->getManager($managerName);

        $manager->flush();
        // need to remove the UploadedFile there otherwise symfony will try to serialize user in this state
        // which have photo->file instance of UploadedFile (could not serialize UploadedFile)
        // Anyway file is uploaded now so we don't need it anymore
        $this->user->getPhoto()->setFile(null);
    }
}
