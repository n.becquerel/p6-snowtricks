<?php

namespace App\Form;

use App\Entity\UserPhoto;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class UserPhotoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, [
                'required' => false,
                'label' => false,
                'constraints' => [
                    new File([
                        'uploadIniSizeErrorMessage' => 'Le fichier ne peut pas dépasser {{ limit }} {{ suffix }}.',
                        'mimeTypes' => ['image/*'],
                        'mimeTypesMessage' => 'Le fichier doit être une image (ex: jpeg, png etc...)',
                    ])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserPhoto::class,
        ]);
    }
}
