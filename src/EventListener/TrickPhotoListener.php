<?php

namespace App\EventListener;

use App\Service\FileUploaderService;

class TrickPhotoListener
{
    /**
     * @var FileUploaderService
     */
    private $uploaderService;

    public function __construct(FileUploaderService $uploaderService)
    {
        $this->uploaderService = $uploaderService;
    }

    public function prePersist($photo)
    {
        $photo->setFilename($this->uploaderService->upload($photo->getFile()));
    }
}
