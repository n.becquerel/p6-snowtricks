<?php

namespace App\EventListener;

use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

class AccessDeniedListener implements EventSubscriberInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;
    /**
     * @var SessionInterface
     */
    private $session;
    /**
     * @var Security
     */
    private $security;

    public function __construct(UrlGeneratorInterface $urlGenerator, SessionInterface $session, Security $security)
    {
        $this->urlGenerator = $urlGenerator;
        $this->session = $session;
        $this->security = $security;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [KernelEvents::EXCEPTION => ['onKernelException', 2]];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        if (!$exception instanceof AccessDeniedException) {
            return;
        }

        /** @var User $user */
        $user = $this->security->getUser();

        if (!$user) {
            $notice = 'Vous devez vous connecter pour accéder à cette page.';
            $redirectRoute = $this->urlGenerator->generate('app_login');
            $this->session->set('_security.main.target_path', $event->getRequest()->getRequestUri());
        }

        if ($user && !$user->getIsVerified()) {
            $notice = 'Pour accéder à cette fonctionnalité votre compte doit être activé.';
            $redirectRoute = $this->urlGenerator->generate('home');
        }


        $this->session->getFlashBag()->add('danger', $notice);
        $event->setResponse(new RedirectResponse($redirectRoute));
    }
}
