<?php

namespace App\EventListener;

use App\Entity\Trick;
use DateTime;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\String\Slugger\SluggerInterface;

class TrickListener
{
    /**
     * @var Security
     */
    private $security;
    /**
     * @var SluggerInterface
     */
    private $slugger;

    public function __construct(Security $security, SluggerInterface $slugger)
    {
        $this->security = $security;
        $this->slugger = $slugger;
    }

    public function prePersist(Trick $trick)
    {
        if ($user = $this->security->getUser()) {
            $trick
                ->setCreatedAt(new DateTime())
                ->setCreatedBy($this->security->getUser())
                ->setSlug($this->slugger->slug($trick->getName()));
        }
    }

    public function preUpdate(Trick $trick)
    {
        if ($user = $this->security->getUser()) {
            $trick
                ->setUpdatedBy($user)
                ->setUpdatedAt(new DateTime())
                ->setSlug($this->slugger->slug($trick->getName()));
        }
    }
}
