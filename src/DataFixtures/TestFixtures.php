<?php

namespace App\DataFixtures;

use App\Entity\Trick;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\String\Slugger\SluggerInterface;

class TestFixtures extends Fixture implements FixtureGroupInterface
{
    /**
     * @var SluggerInterface
     */
    private $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        for ($t = 0; $t < 10; $t++) {
            $name = "Dummy trick $t";
            $trick = (new Trick())
                ->setCreatedAt(new DateTime())
                ->setCreatedBy($this->getReference(AppFixtures::TRICK_OWNER))
                ->setDescription('Dummy trick for tests')
                ->setSlug($this->slugger->slug($name))
                ->setName($name)
                ->setTrickGroup($this->getReference(AppFixtures::TRICK_GROUP_GRAB));

            $manager->persist($trick);
        }

        $manager->flush();
    }

    /**
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['test'];
    }
}
