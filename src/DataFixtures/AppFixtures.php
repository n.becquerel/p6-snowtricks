<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Trick;
use App\Entity\TrickGroup;
use App\Entity\TrickPhoto;
use App\Entity\TrickVideo;
use App\Entity\User;
use App\Entity\UserPhoto;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class AppFixtures extends Fixture implements FixtureGroupInterface
{
    public const TRICK_OWNER = 'trick-owner';

    public const TRICK_GROUP_GRAB = 'trick-group-grab';

    public const TRICK_GROUP_FLIP = 'trick-group-flip';

    private const TRICKS = [
        [
            'name' => 'Air',
            'group' => 0,
            'description' => 'Un front jump des plus basiques.',
            'photos' => ['air.jpeg', 'air2.jpeg'],
            'comments' => [1, 2, 5, 3, 5, 6],
            'videos' => []
        ],
        [
            'name' => 'Frontflip',
            'group' => 0,
            'description' => 'Aussi appelé Tamedog. Une rotation vers l\'avant enmenée par l\'épaule du rider.',
            'photos' => ['frontflip.jpeg'],
            'comments' =>  [6, 0, 1],
            'videos' => [
                'https://www.youtube.com/watch?v=xhvqu2XBvI0',
                'https://www.youtube.com/watch?v=gevwK7GxZAQ'
            ]
        ],
        [
            'name' => 'Backflip',
            'group' => 0,
            'description' => 'AKA Wildcat, il s\'agît d\'éffectuer une rotation mais cette fois vers l\'arrière.',
            'photos' => ['backflip.jpeg'],
            'comments' => [3, 2],
            'videos' => [
                'https://www.youtube.com/watch?v=arzLq-47QFA'
            ]
        ],
        [
            'name' => 'Miller flip',
            'description' => 'En snowboard un miller flip est une variante du frontflip en posant la main avant' .
                ' sur un obstacle',
            'group' => 0,
            'photos' => ['miller-flip.jpg'],
            'comments' => [6, 0, 5],
            'videos' => [
                'https://www.youtube.com/watch?v=nCFkNIaL7yM',
                'https://www.youtube.com/watch?v=Gc7PU9Y9DMg',
                'https://www.youtube.com/watch?v=jXiv_z6kOJI'
            ]
        ],
        [
            'name' => 'Backside air',
            'description' => 'Le trick emblématique du snowboard. La photo parle d\'elle même !',
            'group' => 0,
            'photos' => ['backside-air.jpg'],
            'comments' => [7],
            'videos' => [
                'https://www.youtube.com/watch?v=_CN_yyEn78M' => '_CN_yyEn78M'
            ]
        ],
        [
            'name' => 'Mute',
            'description' => 'Un mute est un grab particuler ou il vous faudra attraper votre planche ',
            'group' => 1,
            'photos' => ['mute-grab.jpg'],
            'comments' => [],
            'videos' => [
                'https://www.youtube.com/watch?v=xhvqu2XBvI0',
                'https://www.youtube.com/watch?v=gevwK7GxZAQ'
            ]
        ],
        [
            'name' => 'Nose grab',
            'description' => 'Le nose grab est un grab. La zone pour attraper la planche se trouve tout simplement' .
                ' à l\'avant de la planche.',
            'group' => 1,
            'photos' => ['Nose-grab.jpg'],
            'comments' => [4, 1, 0, 5],
            'videos' => [
                'https://www.youtube.com/watch?v=gZFWW4Vus-Q'
            ]
        ],
        [
            'name' => 'Tail grab',
            'description' => 'Le tail grab, comme son nom l\'indique est un grab qui consiste a attraper l\arrière' .
                ' de votre planche',
            'group' => 1,
            'photos' => ['tail-grab.jpg'],
            'comments' => [0],
            'videos' => [
                'https://www.youtube.com/watch?v=id8VKl9RVQw'
            ]
        ],
        [
            'name' => 'FS cork 360',
            'description' => 'Ici il s\'agît de faire un frontside flip en y intégrant une rotation de 360° et un' .
                ' cork.',
            'group' => 0,
            'photos' => ['FScork360.jpeg'],
            'comments' => [3, 4],
            'videos' => []
        ],
        [
            'name' => 'BS cork 360',
            'description' => 'Ici il s\'agît de faire un backside flip en y intégrant une rotation de 360° et un' .
                ' cork.',
            'group' => 0,
            'photos' => ['BScork360.jpeg'],
            'comments' => [1, 0, 7],
            'videos' => []
        ]
    ];

    /**
     * Indexes from 0 to 7
     */
    private const TYPICAL_COMMENTS = [
        'Super article !',
        'Je suis nouveau, j\'aime beaucoup le concept',
        'Belle photo d\'illustration',
        'Merci pour cet article',
        'La figure est trop stylée',
        'J\'ai réussi cette figure grâce a ton guide, merci !',
        'Toujours aussi utile de consulter tes tutos pour un débutant comme moi !',
        'Enorme !!!'
    ];

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var SluggerInterface
     */
    private $slugger;
    /**
     * @var string
     */
    private $targetDir;

    public function __construct(UserPasswordEncoderInterface $encoder, SluggerInterface $slugger, string $targetDir)
    {
        $this->encoder = $encoder;
        $this->slugger = $slugger;
        $this->targetDir = $targetDir;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        // users
        $main = new User();
        $main->setEmail('main@snowtricks.com')
            ->setPassword($this->encoder->encodePassword($main, 'mainpassword'))
            ->setIsVerified(true)
            ->setPhoto((new UserPhoto())->setFile($this->createUploadedFile('user-photo-placeholder.png')));
        $manager->persist($main);

        $alt = new User();
        $alt->setEmail('alt@snowtricks.com')
            ->setPassword($this->encoder->encodePassword($alt, 'altpassword'))
            ->setIsVerified(true);
        $manager->persist($alt);

        $disabled = new User();
        $disabled->setEmail('disabled@snowtricks.com')
            ->setPassword($this->encoder->encodePassword($disabled, 'disabledpassword'))
            ->setIsVerified(false)
            ->setValidationToken('gBq7hsNbi91OKeT8INSzl811laX6kZQpsPRoQ9x1L8g');
        $manager->persist($disabled);

        $changePasswordUser = new User();
        $changePasswordUser->setEmail('changepass@snowtricks.com')
            ->setPassword($this->encoder->encodePassword($disabled, 'disabledpassword'))
            ->setIsVerified(true)
            ->setResetPasswordToken('gBq7hsNbi91OKeT8INSzl811laX6kZQpsPRoQ9x1L8g');
        $manager->persist($changePasswordUser);

        $manager->flush();

        // owner for all the initials dataset of the app
        $maintainer = new User();
        $maintainer->setIsVerified(true)
            ->setPassword($this->encoder->encodePassword($maintainer, 'password'))
            ->setEmail('maintainer@snowtricks.com')
            ->setFirstname('Jojn')
            ->setLastname('Maintainer');
        $manager->persist($maintainer);
        $this->addReference(self::TRICK_OWNER, $maintainer);

        // owner for all comments
        $user = new User();
        $user->setIsVerified(true)
            ->setPassword($this->encoder->encodePassword($user, 'password'))
            ->setEmail('commenteur@snowtricks.com')
            ->setFirstname('Comm')
            ->setLastname('Entaire');
        $manager->persist($user);

        // trick groups
        $flipGroup = (new TrickGroup())->setLabel('Flips');
        $manager->persist($flipGroup);
        $this->addReference(self::TRICK_GROUP_FLIP, $flipGroup);
        $grabGroup = (new TrickGroup())->setLabel('Grabs');
        $manager->persist($grabGroup);
        $this->addReference(self::TRICK_GROUP_GRAB, $grabGroup);

        // tricks plus comment
        foreach (self::TRICKS as $trickAr) {
            $trick = (new Trick())->setName($trickAr['name'])
                ->setDescription($trickAr['description'])
                ->setCreatedAt(new DateTime())
                ->setCreatedBy($maintainer)
                ->setSlug($this->slugger->slug($trickAr['name']))
                ->setTrickGroup($trickAr['group'] === 0 ? $flipGroup : $grabGroup)
            ;
            $manager->persist($trick);

            foreach ($trickAr['photos'] as $photoPath) {
                $photo = (new TrickPhoto())
                    ->setTrick($trick)
                    ->setFile($this->createUploadedFile($photoPath));
                $manager->persist($photo);
            }

            foreach ($trickAr['comments'] as $commentIndex) {
                $comment = (new Comment())->setCreatedBy($user)
                    ->setCreatedAt(new DateTime())
                    ->setMessage(self::TYPICAL_COMMENTS[$commentIndex])
                    ->setTrick($trick);
                $manager->persist($comment);
            }

            foreach ($trickAr['videos'] as $fullUrl) {
                $trickVid = (new TrickVideo())->setFullUrl($fullUrl)->setTrick($trick);
                $manager->persist($trickVid);
            }
        }

        $manager->flush();
    }

    /**
     * @return string[]
     */
    public static function getGroups(): array
    {
        return ['app', 'test'];
    }

    private function createUploadedFile(string $photoPath)
    {
        $fs = new Filesystem();
        $extension = substr($photoPath, strpos($photoPath, '.'));
        $fs->copy($this->targetDir . '/' . $photoPath, $this->targetDir . '/temp' . $extension, true);

        return new UploadedFile(
            $this->targetDir . '/temp' . $extension,
            $this->targetDir . '/' . $photoPath,
            'image/' . str_replace('.', '', $extension),
            null,
            true
        );
    }
}
